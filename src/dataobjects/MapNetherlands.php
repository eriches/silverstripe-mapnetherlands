<?php

namespace Hestec\MapNetherlands;

use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\TreeDropdownField;

class MapNetherlands extends DataObject {

    private static $singular_name = 'MapNetherlands';
    private static $plural_name = 'MapNetherlands';

    private static $table_name = 'MapNetherlands';

    private static $db = array(
        'Name' => 'Varchar(255)'
    );

    private static $has_one = array(
        'LinkDrenthe' => SiteTree::class,
        'LinkFlevoland' => SiteTree::class,
        'LinkFriesland' => SiteTree::class,
        'LinkGelderland' => SiteTree::class,
        'LinkGroningen' => SiteTree::class,
        'LinkLimburg' => SiteTree::class,
        'LinkNoordBrabant' => SiteTree::class,
        'LinkNoordHolland' => SiteTree::class,
        'LinkOverijssel' => SiteTree::class,
        'LinkUtrecht' => SiteTree::class,
        'LinkZeeland' => SiteTree::class,
        'LinkZuidHolland' => SiteTree::class
    );

    private static $summary_fields = array(
        'Name'
    );

    public function getCMSFields() {

        $NameField = TextField::create('Name', 'Name');
        $LinkDrentheField = TreeDropdownField::create('LinkDrentheID', "LinkDrenthe", SiteTree::class);
        $LinkFlevolandField = TreeDropdownField::create('LinkFlevolandID', "LinkFlevoland", SiteTree::class);
        $LinkFrieslandField = TreeDropdownField::create('LinkFrieslandID', "LinkFriesland", SiteTree::class);
        $LinkGelderlandField = TreeDropdownField::create('LinkGelderlandID', "LinkGelderland", SiteTree::class);
        $LinkGroningenField = TreeDropdownField::create('LinkGroningenID', "LinkGroningen", SiteTree::class);
        $LinkLimburgField = TreeDropdownField::create('LinkLimburgID', "LinkLimburg", SiteTree::class);
        $LinkNoordBrabantField = TreeDropdownField::create('LinkNoordBrabantID', "LinkNoordBrabant", SiteTree::class);
        $LinkNoordHollandField = TreeDropdownField::create('LinkNoordHollandID', "LinkNoordHolland", SiteTree::class);
        $LinkOverijsselField = TreeDropdownField::create('LinkOverijsselID', "LinkOverijssel", SiteTree::class);
        $LinkUtrechtField = TreeDropdownField::create('LinkUtrechtID', "LinkUtrecht", SiteTree::class);
        $LinkZeelandField = TreeDropdownField::create('LinkZeelandID', "LinkZeeland", SiteTree::class);
        $LinkZuidHollandField = TreeDropdownField::create('LinkZuidHollandID', "LinkZuidHolland", SiteTree::class);

        return new FieldList(
            $NameField,
            $LinkDrentheField,
            $LinkFlevolandField,
            $LinkFrieslandField,
            $LinkGelderlandField,
            $LinkGroningenField,
            $LinkLimburgField,
            $LinkNoordBrabantField,
            $LinkNoordHollandField,
            $LinkOverijsselField,
            $LinkUtrechtField,
            $LinkZeelandField,
            $LinkZuidHollandField
        );

    }

}