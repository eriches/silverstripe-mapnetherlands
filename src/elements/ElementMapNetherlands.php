<?php

namespace Hestec\MapNetherlands;

use DNADesign\Elemental\Models\BaseElement;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\OptionsetField;
use SilverStripe\Forms\DropdownField;

class ElementMapNetherlands extends BaseElement
{

    private static $table_name = 'ElementMapNetherlands';

    private static $singular_name = 'MapNetherlands';

    private static $plural_name = 'MapNetherlands';

    private static $description = 'Adds a Netherlands Map element';

    private static $icon = 'font-icon-p-map';

    private static $db = [
        'Content' => 'HTMLText',
        'MapPosition' => "Enum('left,right', 'right')",
        'Activated' => 'Varchar(4)',
        'InfoText' => 'Varchar(255)'
    ];

    private static $has_one = array(
        'MapNetherlands' => MapNetherlands::class
    );

    public function getCMSFields()
    {

        $fields = parent::getCMSFields();

        $ContentField = HTMLEditorField::create('Content', "Content");
        $ContentField->setRows(5);

        $MapPositionField = OptionsetField::create('MapPosition', "MapPosition", $this->dbObject('MapPosition')->enumValues());
        $MapPositionField->setDescription("Map postition left or right from the text.");

        $MapNetherlandsSource = MapNetherlands::get()->map('ID', 'Name');

        $MapNetherlandsField = DropdownField::create('MapNetherlandsID', "MapNetherlands", $MapNetherlandsSource);
        $MapNetherlandsField->setEmptyString("(select)");

        $InfoTextField = TextField::create('InfoText', "InfoText");
        $InfoTextField->setDescription("Tekst onder de map");

        $ActivatedSource = [
            'nl1' => 'Drenthe',
            'nl2' => 'Flevoland',
            'nl3' => 'Friesland',
            'nl4' => 'Gelderland',
            'nl5' => 'Groningen',
            'nl6' => 'Limburg',
            'nl7' => 'Noord-Brabant',
            'nl8' => 'Noord-Holland',
            'nl9' => 'Overijssel',
            'nl10' => 'Utrecht',
            'nl11' => 'Zeeland',
            'nl12' => 'Zuid-Holland'
        ];

        $ActivatedField = DropdownField::create('Activated', "Activated", $ActivatedSource);
        $ActivatedField->setEmptyString("(select)");

        $fields->addFieldToTab('Root.Main', $ContentField);
        $fields->addFieldToTab('Root.Main', $MapPositionField);
        $fields->addFieldToTab('Root.Main', $MapNetherlandsField);
        $fields->addFieldToTab('Root.Main', $ActivatedField);
        $fields->addFieldToTab('Root.Main', $InfoTextField);

        return $fields;

    }

    public function getType()
    {
        return 'MapNetherlands';
    }

}